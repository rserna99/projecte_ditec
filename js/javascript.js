$(document).ready(function(){
	convertirDivisa();
})


	$("#invertir").click(function(){
    var aux = $("#divisa-base").val();

    $("#divisa-base").val($("#divisa-convertir").val());
	$("#divisa-convertir").val(aux);

	convertirDivisa();
}) 

	
function convertirDivisa(){

	var base = $("#divisa-base").val();

    var quantitat = $("#quantitat").val();
		
    var divisa_convertir = $("#divisa-convertir").val();
    

    if (base == divisa_convertir){
		var resulat = quantitat;


		switch (divisa_convertir){
			case "AUD":
				var simbol = "$";
				break;

			case "BGN":
				var simbol = "Лв";
				break;

			case "BRL":
				var simbol = "Лв";
				break;	

			case "CAD":
				var simbol = "R$";
				break;

			case "CHF": 	
				var simbol = "Fr.";
				break;

			case "CNY":
				var simbol = "¥";
				break;	
				
			case "CZK":
				var simbol = "Kč";
				break;
				
			case "DKK":
				var simbol = "kr";
				break;	

			case "EUR":
				var simbol = "€";
				break;	

			case "GBP":
				var simbol = "kr";
				break;	

			case "HKD":
				var simbol = "£";
				break;

			case "HRK":
				var simbol = "kn";
				break;

			case "HUF":
				var simbol = "Ft";
				break;
				
			case "IDR":
				var simbol = "Rp";
				break;
				
			case "ILS":
				var simbol = "₪";
				break;
				
			case "INR":
				var simbol = "₹";
				break;	

			case "ISK":
				var simbol = "kr";
				break;	

			case "JPY":
				var simbol = "¥";
				break;	

			case "KRW":
				var simbol = "₩";
				break;

			case "MXN":
				var simbol = "$";
				break;

			case "MYR":
				var simbol = "RM";
				break;
				
			case "NOK":
				var simbol = "‎kr";
				break;
				
			case "NZD":
				var simbol = "$";
				break;
				
			case "PHP":
				var simbol = "₱";
				break;	

			case "PLN":
				var simbol = "zł";
				break;	

			case "RON":
				var simbol = "lei";
				break;	

			case "RUB":
				var simbol = "₽";
				break;

			case "SEK":
				var simbol = "kr";
				break;

			case "SGD":
				var simbol = "S$";
				break;
				
			case "THB":
				var simbol = "฿";
				break;
				
			case "TRY":
				var simbol = "try";
				break;

			case "USD":
				var simbol = "$";
				break;	
				
			case "ZAR":
				var simbol = "R";
				break;		
		}
		
		$("#resultat").html(resulat);
		$("#simbol").html(simbol);
		
    } else {
        $.getJSON('https://api.fixer.io/latest?base='+base , function(data){

		switch (divisa_convertir){
			case "AUD":
				var valor = data.rates.AUD;
				var simbol = "$";
				break;

			case "BGN":
				var valor = data.rates.BGN;
				var simbol = "Лв";
				break;

			case "BRL":
				var valor = data.rates.BRL;
				var simbol = "Лв";
				break;	

			case "CAD":
				var valor = data.rates.CAD;
				var simbol = "R$";
				break;

			case "CHF": 	
				var valor = data.rates.CHF;
				var simbol = "Fr.";
				break;

			case "CNY":
				var valor = data.rates.CNY;
				var simbol = "¥";
				break;	
				
			case "CZK":
				var valor = data.rates.CZK;
				var simbol = "Kč";
				break;
				
			case "DKK":
				var valor = data.rates.DKK;
				var simbol = "kr";
				break;	

			case "EUR":
				var valor = data.rates.EUR;
				var simbol = "€";
				break;	

			case "GBP":
				var valor = data.rates.GBP;
				var simbol = "kr";
				break;	

			case "HKD":
				var valor = data.rates.HKD;
				var simbol = "£";
				break;

			case "HRK":
				var valor = data.rates.HRK;
				var simbol = "kn";
				break;

			case "HUF":
				var valor = data.rates.HUF;
				var simbol = "Ft";
				break;
				
			case "IDR":
				var valor = data.rates.IDR;
				var simbol = "Rp";
				break;
				
			case "ILS":
				var valor = data.rates.ILS;
				var simbol = "₪";
				break;
				
			case "INR":
				var valor = data.rates.INR;
				var simbol = "₹";
				break;	

			case "ISK":
				var valor = data.rates.ISK;
				var simbol = "kr";
				break;	

			case "JPY":
				var valor = data.rates.JPY;
				var simbol = "¥";
				break;	

			case "KRW":
				var valor = data.rates.KRW;
				var simbol = "₩";
				break;

			case "MXN":
				var valor = data.rates.MXN;
				var simbol = "$";
				break;

			case "MYR":
				var valor = data.rates.MYR;
				var simbol = "RM";
				break;
				
			case "NOK":
				var valor = data.rates.NOK;
				var simbol = "‎kr";
				break;
				
			case "NZD":
				var valor = data.rates.NZD;
				var simbol = "$";
				break;
				
			case "PHP":
				var valor = data.rates.PHP;
				var simbol = "₱";
				break;	

			case "PLN":
				var valor = data.rates.PLN;
				var simbol = "zł";
				break;	

			case "RON":
				var valor = data.rates.RON;
				var simbol = "lei";
				break;	

			case "RUB":
				var valor = data.rates.RUB;
				var simbol = "₽";
				break;

			case "SEK":
				var valor = data.rates.SEK;
				var simbol = "kr";
				break;

			case "SGD":
				var valor = data.rates.SGD;
				var simbol = "S$";
				break;
				
			case "THB":
				var valor = data.rates.THB;
				var simbol = "฿";
				break;
				
			case "TRY":
				var valor = data.rates.TRY;
				var simbol = "try";
				break;

			case "USD":
				var valor = data.rates.USD;
				var simbol = "$";
				break;	
				
			case "ZAR":
				var valor = data.rates.ZAR;
				var simbol = "R";
				break;		
		}

		var resulat = quantitat * valor;

		resulat = resulat.toFixed(2);

		$("#resultat").html(resulat);
		$("#simbol").html(simbol);

    });
    } 
};